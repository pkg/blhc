blhc for Debian
---------------

Since 0.12 version, blhc is able to ignore false positives spotted by
line(s) "injected" inside .build file via debian/rules. It is useful
for Salsa automated tests and to skip wrong lines shown in local blhc
report. From blhc(1) manpage:

    FALSE POSITIVES
       To suppress false positives you can embed the following string
       in the build log:

           blhc: ignore-line-regexp: REGEXP

       All lines fully matching REGEXP (see --ignore-line for details)
       will be ignored.

       Please use this feature sparingly so that missing flags are not
       overlooked. If you find false positives which affect more packages
       please report a bug.

       To generate this string simply use echo in "debian/rules"; make
       sure to use @ to suppress the echo command itself as it could also
       trigger a false positive.

In the blhc build process, the upstream implemented several tests to
confirm if blhc works fine. These tests can be found in the t/ directory
in source code. Some tests have no all hardening flags deliberately.
Consequently, the command 'blhc --all --debian blhc_<version>.build' will
show some warning lines, but in this case they are false positives because
the intend is verify if blhc detects the absence of these flags. See an
example below:

  $ blhc --all --debian ../blhc_0.12-1_amd64.build

  CFLAGS missing (-g -O2 -fstack-protector-strong -Wformat -Werror=format-security): ok 47 - ./t/logs/ignore-line --ignore-line "\./prepare-script gcc test-[a-z]\.c" --ignore-line "\s*\./prepare-script gcc test-[a-z]\.c .+" (exit code)
  CPPFLAGS missing (-D_FORTIFY_SOURCE=2): ok 47 - ./t/logs/ignore-line --ignore-line "\./prepare-script gcc test-[a-z]\.c" --ignore-line "\s*\./prepare-script gcc test-[a-z]\.c .+" (exit code)
  LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): ok 47 - ./t/logs/ignore-line --ignore-line "\./prepare-script gcc test-[a-z]\.c" --ignore-line "\s*\./prepare-script gcc test-[a-z]\.c .+" (exit code)
  CFLAGS missing (-g -O2 -fstack-protector-strong -Wformat -Werror=format-security): ok 48 - ./t/logs/ignore-line --ignore-line "\./prepare-script gcc test-[a-z]\.c" --ignore-line "\s*\./prepare-script gcc test-[a-z]\.c .+" (output)
  CPPFLAGS missing (-D_FORTIFY_SOURCE=2): ok 48 - ./t/logs/ignore-line --ignore-line "\./prepare-script gcc test-[a-z]\.c" --ignore-line "\s*\./prepare-script gcc test-[a-z]\.c .+" (output)
  LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): ok 48 - ./t/logs/ignore-line --ignore-line "\./prepare-script gcc test-[a-z]\.c" --ignore-line "\s*\./prepare-script gcc test-[a-z]\.c .+" (output)

The solution to blhc (>= 0.12) ignore all false positives is write a line
inside .build file to drop all matches with the following Perl Regexp:

  .*prepare-script\ gcc\ test-\[a-z\]\\\.c.*

See a real example from the debian/rules used in blhc package:

  override_dh_auto_test:
          @echo 'blhc: ignore-line-regexp: .*prepare-script\ gcc\ test-\[a-z\]\\\.c.*'
          dh_auto_test

Note this is not a magic formula. So, you need to understand the issue and
write a rule in a right part of debian/rules (sometimes in dh_auto_build,
sometimes in dh_auto_test, etc.). This technique must be used for FALSE
POSITIVES only (don't use it to override warnings for unsolved but real
hardening issues).

A quick reference about Perl Regexp is here[1].

[1] http://jkorpela.fi/perl/regexp.html

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 29 Jul 2020 20:41:39 -0300
